package com.lastminute.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import com.lastminute.core.util.DateUtils;

public class DateUtilsTest {
	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	
	String correctDate = "dd/MM/yyyy";
	String wrong1Date = "dd-MM-yyyy";
	String wrong2Date = "dd.MM.yyyy";
	
	long correctDays = 0;
	
	@Test
	public void DateUtils_formatDate_correct(){
		assertTrue(DateUtils.formatDate(correctDate).equals(correctDate));
	}
	
	@Test
	public void DateUtils_formatDate_wrong1(){
		assertTrue(DateUtils.formatDate(wrong1Date).equals(correctDate));
	}
	
	@Test
	public void DateUtils_formatDate_wrong2(){
		assertTrue(DateUtils.formatDate(wrong2Date).equals(correctDate));
	}
	
	@Test
	public void DateUtils_getDays_correct(){
		
		DateTime date = new DateTime();
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
		assertTrue(DateUtils.getDays(formatter.print(date)) == correctDays);
	}
	
	@Test
	public void DateUtils_getDays_exception(){
		String dateDeparture = "exception";
		assertEquals(DateUtils.getDays(dateDeparture),1);
	}
	
	
	
}
