package com.lastminute.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.lastminute.core.dto.request.SearchFlightDTO;
import com.lastminute.core.dto.response.FlightResultDTO;
import com.lastminute.service.searchflight.impl.SearchFlightService;

public class SearchFlightServiceTest {
	
	SearchFlightService fileStatusService = new SearchFlightService();
	
	private SearchFlightDTO setParamsNoDepartureDate(){
		return new SearchFlightDTO();
	}
	
	private SearchFlightDTO setParamsNoDestiny(){
		SearchFlightDTO result = new SearchFlightDTO();
		result.setDateDeparture("31/01/2018");
		
		return result;
	}
	
	private SearchFlightDTO setParamsNoOrigin(){
		SearchFlightDTO result = new SearchFlightDTO();
		result.setDateDeparture("31/01/2018");
		result.setDestiny("***");
		
		return result;
	}
	
	private SearchFlightDTO setParamsNoPassengers(){
		SearchFlightDTO result = new SearchFlightDTO();
		result.setDateDeparture("31/01/2018");
		result.setDestiny("***");
		result.setOrigin("***");
		
		return result;
	}
	
	private SearchFlightDTO setParams(){
		SearchFlightDTO result = new SearchFlightDTO();
		result.setDateDeparture("31/01/2018");
		result.setDestiny("***");
		result.setOrigin("***");
		result.setPassengers(3);
		
		return result;
	}
	
	private SearchFlightDTO setParams_result(){
		SearchFlightDTO result = new SearchFlightDTO();
		result.setOrigin("CPH");
		result.setDestiny("FRA");
		result.setPassengers(3);
		result.setDateDeparture("31/12/2017");
		
		return result;
	}
	
	@Test(expected=Exception.class)
	public void findFlights_paramsEmpty() throws Exception{
		assertEquals(fileStatusService.findFlights(null), null);
	}
	
	@Test(expected=Exception.class)
	public void findFlights_dateDepartureNull() throws Exception{
		fileStatusService.findFlights(setParamsNoDepartureDate());
	}
	
	@Test(expected=Exception.class)
	public void findFlights_destinyNull() throws Exception{
		fileStatusService.findFlights(setParamsNoDestiny());
	}
	
	@Test(expected=Exception.class)
	public void findFlights_originNull() throws Exception{
		fileStatusService.findFlights(setParamsNoOrigin());
	}
	
	@Test(expected=Exception.class)
	public void findFlights_passengers0() throws Exception{
		fileStatusService.findFlights(setParamsNoPassengers());
	}
	
	@Test
	public void findFlights_withParams() throws Exception{
		assertTrue(fileStatusService.findFlights(setParams()).isEmpty());
	}
	
	@Test
	public void findFlights_withParams_result() throws Exception{
		assertTrue(!fileStatusService.findFlights(setParams_result()).isEmpty());
	}

}
