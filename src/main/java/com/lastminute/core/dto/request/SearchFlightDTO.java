package com.lastminute.core.dto.request;

public class SearchFlightDTO {
	
	private String origin;
	private String destiny;
	private String dateDeparture;
	private int passengers;
	
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestiny() {
		return destiny;
	}
	public void setDestiny(String destiny) {
		this.destiny = destiny;
	}
	public String getDateDeparture() {
		return dateDeparture;
	}
	public void setDateDeparture(String dateDeparture) {
		this.dateDeparture = dateDeparture;
	}
	public int getPassengers() {
		return passengers;
	}
	public void setPassengers(int passengers) {
		this.passengers = passengers;
	}
}
